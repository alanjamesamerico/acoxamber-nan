package br.com.estatistic.acoxambration.core;
	
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;


public class Main extends Application {
	
    private AnchorPane root;
    
    @Override
	public void start(Stage stage) {
		try {
		    
            FXMLLoader loader = new FXMLLoader();
		    loader.setLocation(Main.class.getResource("fxml/scatterChart.fxml"));
		    
			root = loader.load();
			
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("css/application.css").toExternalForm());
			
			stage.setScene(scene);
			stage.setTitle("Acoxamber NAN 2.0");
			stage.show();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}