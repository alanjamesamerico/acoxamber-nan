package br.com.estatistic.acoxambration.core.utils;

import java.util.List;

import br.com.estatistic.acoxambration.core.model.VariableCorrelation;

public class CalculationUtil {
    
    public static double SUM = 0;
    
    public static double calculateCorrelationCoefficient( List<VariableCorrelation> variables ) {
        
        int n = variables.size();           // n�mero de observa��es
        double sumX     = sumX(variables);  // Somat�rio de X
        double sumY     = sumY(variables);  // Somat�rio de Y
        double sumXY    = sumXY(variables); // Somat�rio de X * Y
        double sumX2    = sumX2(variables); // Somat�rio de X ao quadrado
        double sumY2    = sumY2(variables); // Somat�rio de Y ao quadrado
        
        double numerator;                   // Numerador
        double denominator;                 // Denominador
        double r;                           // Coeficiente de Pearson
        
        numerator = (n * sumXY) - (sumX * sumY);
        denominator = Math.sqrt( (((n * sumX2) - (Math.pow(sumX, 2))) * ((n * sumY2) - (Math.pow(sumY, 2)))) );
        
        r = numerator / denominator;
        
        return r; // retorna o valor do coeficiente de Pearson
    }
    
    @SuppressWarnings("unused")
    private static boolean isDivisionValid(double numerator, double denominator) {
        
        try {
            
            double result = numerator/denominator;
            return true;
        }
        catch (ArithmeticException e) {
            return false;
        }
    }

    public static double sumX (List<VariableCorrelation> variables) {
        
        SUM = 0;
        for (VariableCorrelation variable : variables) {
            SUM = SUM + variable.getVCorrelationX();
        }
        
        return SUM;
    }
    
    public static double sumY (List<VariableCorrelation> variables) {
        
        SUM = 0;
        for (VariableCorrelation variable : variables) {
            SUM = SUM + variable.getVCorrelationY();
        }
        
        return SUM;
    }
    
    public static double sumX2 (List<VariableCorrelation> variables) {
        
    	SUM = 0;
        for (VariableCorrelation variable : variables) {
            SUM = SUM + (variable.getVCorrelationX() * variable.getVCorrelationX());
        }
        
        return SUM;
    }
    
    public static double sumY2 (List<VariableCorrelation> variables) {
    	
    	SUM = 0;
        for (VariableCorrelation variable : variables) {
            SUM = SUM + (variable.getVCorrelationY() * variable.getVCorrelationY());
        }
        
        return SUM;
    }
    
    public static double sumXY (List<VariableCorrelation> variables) {
        
    	SUM = 0;
        for (VariableCorrelation variable : variables) {
            SUM = SUM + (variable.getVCorrelationX() * variable.getVCorrelationY());
        }
        
        return SUM;
    }
    
    public static String degreeCorrelation(double r) {
    	
    	String grauCorrelation = "";
    	
    	if ( r >= 0.6 && r <= 1) {
    	
    		grauCorrelation = "Forte";
    	
    	} else if(r >= 0.3 && r < 0.6) {
    		
    		grauCorrelation = "Fraca"; 
    	
    	} else if (r >= 0 && r < 0.3) {
    		
    		grauCorrelation = "Muito Fraca";
    		
    	} else {
    		grauCorrelation = "Forte";
    	}
    	
    	return grauCorrelation;
    }
    
    public String checkCorrelation(double r) {
    	
    	String correlation = "";
    	
    	if (r == 1) {
    		correlation = "Positiva Perfeita";
    	} else if (r == 0){
    		correlation = "Nula";
    	} else if (r == -1){
    		correlation = "Negativa Perfeita";
    	}
    	
    	return correlation;
    }
}
