package br.com.estatistic.acoxambration.core.controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.commons.lang3.StringUtils;

import br.com.estatistic.acoxambration.core.model.VariableCorrelation;
import br.com.estatistic.acoxambration.core.utils.CalculationUtil;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.paint.Color;

public class AcoxamberNanController implements Initializable {
    
    // Vari�veis de controle para o Gr�fico
    @FXML private ScatterChart<Number, Number> scatterChart;
    @FXML private NumberAxis xAxis;
    @FXML private NumberAxis yAxis;
    
    // Vari�veis de controle para a Tabela
    @FXML private TableView<VariableCorrelation> tableVariable;
    @FXML private TableColumn<VariableCorrelation, Double> columnX;
    @FXML private TableColumn<VariableCorrelation, Double> columnY; 
    @FXML private TextField txVariableX;
    @FXML private TextField txVariableY;
    @FXML private Label lbTotal;
    
    @FXML private Label lbNumberObservation;
    @FXML private Label lbDegreeCorrelation;
    @FXML private Label lbPearsonCoefficient;
    @FXML private Label lbGeneral;
    @FXML private Label lbVariable;
    
    @SuppressWarnings("rawtypes") 
    private XYChart.Series serie = new XYChart.Series();
    
    private List<VariableCorrelation> 	variables 	= new ArrayList<VariableCorrelation>();
    
    @SuppressWarnings("unchecked")
    public void initialize(URL arg0, ResourceBundle arg1) {
        
        tableVariable.setEditable(true);
        
        scatterChart.setTitle("Vari�veis Correlacionadas");
        scatterChart.getData().addAll(serie);
        
        xAxis.setLabel("Anos");
        yAxis.setLabel("Meses");
        
        serie.setName("Exemplo Acoxambrado de vari�veis correlacionadas");
    }
    
    @FXML
    public void onMousePressedVariableX() {
        if(!StringUtils.isBlank(lbVariable.getText()) || !StringUtils.isBlank(lbGeneral.getText()) ) {
            lbVariable.setText("");
            lbGeneral.setText("");
        }
    }
    
    @FXML
    public void onMousePressedVariableY() {
        if(!StringUtils.isBlank(lbVariable.getText()) || !StringUtils.isBlank(lbGeneral.getText())) {
            lbVariable.setText("");
            lbGeneral.setText("");
        }
    }
    
    @FXML
    public void btnAddVariable(ActionEvent event) {
        
        lbVariable.setText("");
        
        if (isVariableValid()) {
            
            double variableX = Double.parseDouble(txVariableX.getText());
            double variableY = Double.parseDouble(txVariableY.getText());
            
            txVariableX.clear();
            txVariableY.clear();
            
            variables.add(new VariableCorrelation(variableX, variableY));
            
            columnX.setCellValueFactory(new PropertyValueFactory<VariableCorrelation, Double>("vCorrelationX"));
            
//            columnX.setOnEditCommit(new EventHandler<CellEditEvent<VariableCorrelation, SimpleDoubleProperty>>(){
//                public void handle(CellEditEvent<VariableCorrelation, SimpleDoubleProperty> t) {
//                    
//                    ( (VariableCorrelation) t.getTableView().getItems().get(
//                                    t.getTablePosition()
//                                    .getRow()))
//                                    .setVCorrelationX(t.getNewValue());
//                }
//            });
            
            columnY.setCellValueFactory(new PropertyValueFactory<VariableCorrelation, Double>("vCorrelationY"));
            
            tableVariable.setItems(FXCollections.observableArrayList(variables));
            
        } else {
            lbVariable.setText("Insira um valor v�lido !");
            lbVariable.setTextFill(Color.RED);
        }
    }

    @FXML
    public void btnRemoveVariable(ActionEvent event) {
        
        int lastElement = variables.size()-1;
        
        if(lastElement >= 0) { 
        	
        	variables.remove(lastElement);
        	tableVariable.setItems(FXCollections.observableArrayList(variables));
        	
        	if (!(serie.getData().size() == 0)) {
        	    serie.getData().remove(lastElement);
        	}
        }
    }
    
   @FXML
   public void btnCalculateCorrelationCoefficient(ActionEvent event) {
       
	   if (variables.size() > 0) {
		   
		   double r = CalculationUtil.calculateCorrelationCoefficient(variables);
		   
		   if (StringUtils.equals(Double.toString(r), "NaN")) {
		       
		       lbPearsonCoefficient.setTextFill(Color.RED);
		       lbPearsonCoefficient.setText("Erro.");
		   
		   } else {
		       
		       String rFormated = String.format("%.2f", r).replace(",", "."); // formata apenas para duas casas decimais e substituindo "," por "."
		       
		       lbNumberObservation.setText(Integer.toString(variables.size()));
		       lbDegreeCorrelation.setText(CalculationUtil.degreeCorrelation(Double.parseDouble(rFormated)));
		       lbPearsonCoefficient.setText(rFormated);
		   }
		   
		   
	   } else {
	       
	       lbGeneral.setText("Favor adicionar valores as vari�veis");
	       lbGeneral.setTextFill(Color.RED);
	   }
   }
   
   @FXML
   @SuppressWarnings({ "unchecked", "rawtypes" })
   public void btnPlotVariables(ActionEvent event) {
		for (VariableCorrelation v : this.variables) {
			serie.getData().add(new XYChart.Data(v.getVCorrelationX(), v.getVCorrelationY()));
		}
   }
   
   @FXML
   public void btnResetDatas (ActionEvent event) {
       
       int sizeList = variables.size();
       
       if(sizeList > 0 && !serie.getData().isEmpty()) { 
           for (int i = sizeList - 1; i >= 0; i--) {
               serie.getData().remove(i);
           }
       }
       
       variables.removeAll(variables);
       tableVariable.setItems(FXCollections.observableArrayList(variables));
       
       txVariableX.clear();
       txVariableY.clear();
       
       lbNumberObservation.setText("");
       lbDegreeCorrelation.setText("");
       lbPearsonCoefficient.setText("");
       lbGeneral.setText("");
       lbVariable.setText("");
   }

    private boolean isVariableValid() {
        
        boolean valid = true;
        boolean variableX = StringUtils.isBlank(txVariableX.getText());
        boolean variableY = StringUtils.isBlank(txVariableY.getText());
        
        if (variableX || variableY)     { valid = false; } 
        else if (!isVariableXValid())   { valid = false; } 
        else if (!isVariableYValid())   { valid = false; }
        
        return valid;
    }

    private boolean isVariableYValid() {
        
        boolean valid;
        try {
            Double.parseDouble(txVariableY.getText());
            valid = true;
        }
        catch (Exception e) { valid = false; }
        return valid;
    }

    private boolean isVariableXValid() {
    
        boolean valid;
        try {
            Double.parseDouble(txVariableX.getText());
            valid = true;
        }
        catch (Exception e) { valid = false; }
        return valid;
    }
}
