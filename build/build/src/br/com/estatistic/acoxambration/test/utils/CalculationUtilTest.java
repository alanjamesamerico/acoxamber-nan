package br.com.estatistic.acoxambration.test.utils;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import br.com.estatistic.acoxambration.core.model.VariableCorrelation;
import br.com.estatistic.acoxambration.core.utils.CalculationUtil;

public class CalculationUtilTest {
	
	private CalculationUtil cUtil = new CalculationUtil();
	
	private static final List<VariableCorrelation> variables = Arrays.asList(
			new VariableCorrelation(5, 6),
			new VariableCorrelation(8, 9),
			new VariableCorrelation(7, 8),
			new VariableCorrelation(10, 10),
			new VariableCorrelation(6, 5),
			new VariableCorrelation(7, 7),
			new VariableCorrelation(9, 8),
			new VariableCorrelation(3, 4),
			new VariableCorrelation(8, 6),
			new VariableCorrelation(2, 2));
	
	
    @Test
    @SuppressWarnings("static-access")
	public void calculateCorrelationCoefficient() {
		
		System.out.println("sum X " + cUtil.sumX(variables));
		System.out.println("sum Y " + cUtil.sumY(variables));
		System.out.println("sum X2 " + cUtil.sumX2(variables));
		System.out.println("sum Y2 " + cUtil.sumY2(variables));
		System.out.println("sum XY " + cUtil.sumXY(variables));
		
		System.out.println("\nr = " + cUtil.calculateCorrelationCoefficient(variables));
	}
	
    @Test
    public void divTest(){
        System.out.println("Indeterminado: " + 0/0);
        System.out.println("Inexistente: " + 2/0);
    }
    
    public static void main(String[] args) {
        System.out.println("Indeterminado: " + 0/0);
        System.out.println("Inexistente: " + 2/0);
    }

}
