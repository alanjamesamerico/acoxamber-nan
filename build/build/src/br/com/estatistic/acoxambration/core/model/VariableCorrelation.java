package br.com.estatistic.acoxambration.core.model;

import javafx.beans.property.SimpleDoubleProperty;

public class VariableCorrelation {
    
    private SimpleDoubleProperty vCorrelationX;
    private SimpleDoubleProperty vCorrelationY;
    
    public VariableCorrelation(double vCorrelationX, double vCorrelationY) {
        
        this.vCorrelationX = new SimpleDoubleProperty(vCorrelationX);
        this.vCorrelationY = new SimpleDoubleProperty(vCorrelationY);
    }
    
    // Getters and Setters
    
    public double getVCorrelationX() {
        return vCorrelationX.get();
    }

    public void setVCorrelationX(SimpleDoubleProperty vCorrelationX) {
        this.vCorrelationX = vCorrelationX;
    }

    public double getVCorrelationY() {
        return vCorrelationY.get();
    }

    public void setVCorrelationY(SimpleDoubleProperty vCorrelationY) {
        this.vCorrelationY = vCorrelationY;
    }
}
